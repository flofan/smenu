SMenu.frame.config.main = SMenu.frame.config.main or {}

function SMenu.frame.config.main.Show()

	SMenu.frame.config.mainframe = vgui.Create("SFrame")
	SMenu.frame.config.mainframe:SetSize(ScrW() * 0.2, ScrH() * 0.5)
	SMenu.frame.config.mainframe:Center()
	SMenu.frame.config.mainframe:MakePopup()
	SMenu.frame.config.mainframe:SetTitle(SMenu.Language["Parameters"])

	local mfx, mfy = SMenu.frame.config.mainframe:GetSize()

	SMenu.frame.config.mainframe_categorybutton = vgui.Create("SMenuButton", SMenu.frame.config.mainframe )
	SMenu.frame.config.mainframe_categorybutton:SetText(SMenu.Language["Category"])
	SMenu.frame.config.mainframe_categorybutton:SetSize(mfx * 0.5, mfy * 0.1)
	SMenu.frame.config.mainframe_categorybutton:CenterVertical(0.25)
	SMenu.frame.config.mainframe_categorybutton:CenterHorizontal(0.5)
	function SMenu.frame.config.mainframe_categorybutton:DoClick()
		SMenu.frame.config.category.Show()
	end

	SMenu.frame.config.mainframe_categorytool = vgui.Create("SMenuButton", SMenu.frame.config.mainframe )
	SMenu.frame.config.mainframe_categorytool:SetText(SMenu.Language["Tool"])
	SMenu.frame.config.mainframe_categorytool:SetSize(mfx * 0.5, mfy * 0.1)
	SMenu.frame.config.mainframe_categorytool:CenterVertical(0.45)
	SMenu.frame.config.mainframe_categorytool:CenterHorizontal(0.5)
	function SMenu.frame.config.mainframe_categorytool:DoClick()
		SMenu.frame.config.toolgun.Show()
	end

	SMenu.frame.config.mainframe_tabbutton = vgui.Create("SMenuButton", SMenu.frame.config.mainframe )
	SMenu.frame.config.mainframe_tabbutton:SetText(SMenu.Language["Tab"])
	SMenu.frame.config.mainframe_tabbutton:SetSize(mfx * 0.5, mfy * 0.1)
	SMenu.frame.config.mainframe_tabbutton:CenterVertical(0.65)
	SMenu.frame.config.mainframe_tabbutton:CenterHorizontal(0.5)
	function SMenu.frame.config.mainframe_tabbutton:DoClick()
		SMenu.frame.config.tabs.Show()
	end

	SMenu.frame.config.mainframe_tabbutton = vgui.Create("SMenuButton", SMenu.frame.config.mainframe )
	SMenu.frame.config.mainframe_tabbutton:SetText(SMenu.Language["Colors"])
	SMenu.frame.config.mainframe_tabbutton:SetSize(mfx * 0.5, mfy * 0.1)
	SMenu.frame.config.mainframe_tabbutton:CenterVertical(0.85)
	SMenu.frame.config.mainframe_tabbutton:CenterHorizontal(0.5)
	function SMenu.frame.config.mainframe_tabbutton:DoClick()
		SMenu.frame.config.color.Show()
	end
end