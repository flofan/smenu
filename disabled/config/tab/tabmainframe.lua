SMenu = SMenu or {}
SMenu.frame.config = SMenu.frame.config or {}
SMenu.frame.config.tabs = SMenu.frame.config.tabs or {}

function SMenu.frame.config.tabs.ReloadUserGroup(tab)
	SMenu.frame.config.tabs.usergroupList:Clear()
	for _,v in pairs(SMenu.frame.config.tabs.comboboxFrame:GetDataByChoice(tab)) do
		SMenu.frame.config.tabs.usergroupList:AddLine(v)
	end
end

function SMenu.frame.config.tabs.Show()
	SMenu.frame.config.tabs.tabsmainFrame = vgui.Create("SFrame")
	SMenu.frame.config.tabs.tabsmainFrame:MakePopup()
	SMenu.frame.config.tabs.tabsmainFrame:SetTitle(SMenu.Language["Tab"])
	SMenu.frame.config.tabs.tabsmainFrame:SetSize(ScrW()*0.4, ScrH()*0.8)
	SMenu.frame.config.tabs.tabsmainFrame:Center()

	SMenu.frame.config.tabs.usergroupList = vgui.Create("SListView", SMenu.frame.config.tabs.tabsmainFrame)
	SMenu.frame.config.tabs.usergroupList:SetSize(ScrH()* 0.8 * 0.35, ScrW()* 0.8 * 0.3)
	SMenu.frame.config.tabs.usergroupList:CenterHorizontal()
	SMenu.frame.config.tabs.usergroupList:CenterVertical(0.55)
	SMenu.frame.config.tabs.usergroupList:AddColumn(SMenu.Language["Sort"])

	SMenu.frame.config.tabs.comboboxFrame = vgui.Create("SComboBox", SMenu.frame.config.tabs.tabsmainFrame)
	SMenu.frame.config.tabs.comboboxFrame:SetSize(ScrW()* 0.8 * 0.3, ScrH() *0.8 * 0.1)
	SMenu.frame.config.tabs.comboboxFrame:CenterHorizontal()
	SMenu.frame.config.tabs.comboboxFrame:CenterVertical(0.2)

	function SMenu.frame.config.tabs.comboboxFrame:OnSelect(index, value, data)
		SMenu.frame.config.tabs.ReloadUserGroup(value)
	end

	SMenu.frame.config.tabs.comboboxFrame:AddChoice(SMenu.Language["Props"], SMenu.frame.config.tabs.tabs["props"], true)
	SMenu.frame.config.tabs.comboboxFrame:AddChoice(SMenu.Language["Entities"], SMenu.frame.config.tabs.tabs["entities"], false)
	SMenu.frame.config.tabs.comboboxFrame:AddChoice(SMenu.Language["Vehicles"], SMenu.frame.config.tabs.tabs["vehicles"], false)
	SMenu.frame.config.tabs.comboboxFrame:AddChoice(SMenu.Language["Weapons"], SMenu.frame.config.tabs.tabs["weapons"], false)

	SMenu.frame.config.tabs.addButton = vgui.Create("SMenuButton", SMenu.frame.config.tabs.tabsmainFrame)
	SMenu.frame.config.tabs.addButton:SetSize( ScrW()* 0.8 * 0.2,ScrH()* 0.8 * 0.05)
	SMenu.frame.config.tabs.addButton:CenterHorizontal(0.25)
	SMenu.frame.config.tabs.addButton:CenterVertical(0.87)
	SMenu.frame.config.tabs.addButton.HoverColor = SMenu.Color.GetColor("green")
	SMenu.frame.config.tabs.addButton:SetText(SMenu.Language["Add"])
	function SMenu.frame.config.tabs.addButton:DoClick()
		SMenu.frame.config.tabs.tabsmainFrame:SetMouseInputEnabled(false)
		SMenu.frame.config.tabs.tabsmainFrame:SetKeyboardInputEnabled(false)
		SMenu.frame.popup.AskText(SMenu.Language["AskUserGroup"], function (res, pnl)
			if(IsValid(SMenu.frame.config.tabs.tabsmainFrame)) then
				SMenu.frame.config.tabs.usergroupList:AddLine(res)
				local selected = SMenu.frame.config.tabs.comboboxFrame:GetSelected()
				local data = SMenu.frame.config.tabs.comboboxFrame:GetDataByChoice(selected)
				table.insert(data, res)
			else
				pnl:Close()
				SMenu.frame.popup.Info(SMenu.Language["PanelDoesntExist"])
			end
		end, function ()
			SMenu.frame.config.tabs.tabsmainFrame:SetMouseInputEnabled(true)
			SMenu.frame.config.tabs.tabsmainFrame:SetKeyboardInputEnabled(true)
		end)
	end

	SMenu.frame.config.tabs.removeButton = vgui.Create("SMenuButton", SMenu.frame.config.tabs.tabsmainFrame)
	SMenu.frame.config.tabs.removeButton:SetSize( ScrW()* 0.8 * 0.2,ScrH()* 0.8 * 0.05)
	SMenu.frame.config.tabs.removeButton:CenterHorizontal(0.75)
	SMenu.frame.config.tabs.removeButton:CenterVertical(0.87)
	SMenu.frame.config.tabs.removeButton.HoverColor = SMenu.Color.GetColor("red")
	SMenu.frame.config.tabs.removeButton:SetText(SMenu.Language["Remove"])
	function SMenu.frame.config.tabs.removeButton:DoClick()
		local selectedLines = SMenu.frame.config.tabs.usergroupList:GetSelected()
		local tab = SMenu.frame.config.tabs.comboboxFrame:GetSelected()
		local data = SMenu.frame.config.tabs.comboboxFrame:GetDataByChoice(tab)
		for _, v in pairs(selectedLines) do
			table.RemoveByValue(SMenu.frame.config.tabs.usergroupList.Lines, v)
			table.RemoveByValue(SMenu.frame.config.tabs.usergroupList.Sorted, v )
			table.RemoveByValue(data, v:GetColumnText(1))

			SMenu.frame.config.tabs.usergroupList:SetDirty( true )
			SMenu.frame.config.tabs.usergroupList:InvalidateLayout()

			v:Remove()
		end
		SMenu.frame.config.tabs.comboboxFrame:SetDataChoice(tab, data)
	end

	SMenu.frame.config.tabs.saveButton = vgui.Create("SMenuButton", SMenu.frame.config.tabs.tabsmainFrame)
	SMenu.frame.config.tabs.saveButton:SetSize( ScrW()* 0.8 * 0.2,ScrH()* 0.8 * 0.05)
	SMenu.frame.config.tabs.saveButton:CenterHorizontal(0.5)
	SMenu.frame.config.tabs.saveButton:CenterVertical(0.94)
	SMenu.frame.config.tabs.saveButton.HoverColor = SMenu.Color.GetColor("green")
	SMenu.frame.config.tabs.saveButton:SetText(SMenu.Language["Save"])
	function SMenu.frame.config.tabs.saveButton:DoClick()
		SMenu.frame.config.tabs.ModifyTab("props", SMenu.frame.config.tabs.comboboxFrame:GetDataByChoice(SMenu.Language["Props"]))
		SMenu.frame.config.tabs.ModifyTab("entities", SMenu.frame.config.tabs.comboboxFrame:GetDataByChoice(SMenu.Language["Entities"]))
		SMenu.frame.config.tabs.ModifyTab("vehicles", SMenu.frame.config.tabs.comboboxFrame:GetDataByChoice(SMenu.Language["Vehicles"]))
		SMenu.frame.config.tabs.ModifyTab("weapons", SMenu.frame.config.tabs.comboboxFrame:GetDataByChoice(SMenu.Language["Weapons"]))
		SMenu.frame.config.tabs.SendToGlobalConfig()
		SMenu.frame.config.tabs.tabsmainFrame:SetMouseInputEnabled(false)
		SMenu.frame.config.tabs.tabsmainFrame:SetKeyboardInputEnabled(false)
		SMenu.frame.popup.Info(SMenu.Language["SuccessSave"], function()
			SMenu.frame.config.tabs.tabsmainFrame:SetMouseInputEnabled(true)
			SMenu.frame.config.tabs.tabsmainFrame:SetKeyboardInputEnabled(true)
		end)
	end
end