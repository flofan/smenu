SMenu = SMenu or {}
SMenu.frame.popup = SMenu.frame.popup or {}

function SMenu.frame.popup.Confirm(question, callback, closeCallback)

	surface.SetFont("SMenu:Title")
	local tx, ty = surface.GetTextSize(question)

	local popup = vgui.Create("SFrame")
	popup:SetDrawOnTop(true)
	popup:MakePopup()
	popup:SetTitle(SMenu.Language["Confirm"])
	popup:SetSize(math.Clamp(tx * 1.2, 500, 1000), 300 )
	popup:Center()
	function popup:Paint(w, h)
		draw.RoundedBox(0, 0, 0, w, h, SMenu.Color.GetColor("secondary"))
		draw.RoundedBox(0, 0, 0, w, 50, SMenu.Color.GetColor("black"))
	end

	if(closeCallback != nil) then
		function popup:OnClose() 
			closeCallback()
		end
	end

	function popup:Think()
		popup:MoveToFront()
	end

	function popup:OnFocusChanged(gained) 
		popup:RequestFocus()
	end

	local textLabel = vgui.Create("SLabel", popup)
	textLabel:SetSize(tx, ty)
	textLabel:Center()
	textLabel:SetText(question)

	local buttonValidate = vgui.Create("SMenuButton", popup)
	buttonValidate:SetSize(200, 50)
	buttonValidate:SetPos(200, 225)
	buttonValidate:SetText(SMenu.Language["Yes"])
	buttonValidate.HoverColor = SMenu.Color.GetColor("green")
	buttonValidate:CenterHorizontal(0.3)
	function buttonValidate:DoClick()
		callback(popup)
		popup:Close()
	end

	local buttonCancel = vgui.Create("SMenuButton", popup)
	buttonCancel:SetSize(200, 50)
	buttonCancel:SetPos(200, 225)
	buttonCancel:SetText(SMenu.Language["No"])
	buttonCancel.HoverColor = SMenu.Color.GetColor("red")
	buttonCancel:CenterHorizontal(0.7)
	function buttonCancel:DoClick()
		popup:Close()
	end

	
end