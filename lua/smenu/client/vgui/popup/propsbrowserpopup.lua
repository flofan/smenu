SMenu = SMenu or {}
SMenu.frame.popup = SMenu.frame.popup or {}
SMenu.frame.popup.props = nil
function SMenu.frame.popup.PropPopup(callback, closeCallback)
	surface.SetFont("SMenu:Title")

	if(SMenu.frame.popup.props) then
		SMenu.frame.popup.props:Show()
		SMenu.frame.popup.props:FadeIn()
		SMenu.frame.popup.props:MakePopup()
	else
		SMenu.frame.popup.props = vgui.Create("SFrame")
		SMenu.frame.popup.props:SetDrawOnTop(true)
		SMenu.frame.popup.props:MakePopup()
		SMenu.frame.popup.props:SetTitle(SMenu.Language["AskProp"])
		SMenu.frame.popup.props:SetSize(ScrW() * 0.8, ScrH() * 0.8 )
		SMenu.frame.popup.props:Center()
		SMenu.frame.popup.props:SetDeleteOnClose(false)
		function SMenu.frame.popup.props:Paint(w, h)
			draw.RoundedBox(0, 0, 0, w, h, SMenu.Color.GetColor("secondary"))
			draw.RoundedBox(0, 0, 0, w, 50, SMenu.Color.GetColor("black"))
		end

		SMenu.frame.popup.propsPopup = vgui.Create("SFileBrowser", SMenu.frame.popup.props)

		SMenu.frame.popup.propsPopup:Dock(FILL)
		SMenu.frame.popup.propsPopup:DockMargin(5, 60, 5, 5)

		SMenu.frame.popup.propsPopup:SetPath( "GAME" )
		SMenu.frame.popup.propsPopup:SetBaseFolder( "models" )
		SMenu.frame.popup.propsPopup:SetName( "Props" )
		SMenu.frame.popup.propsPopup:SetFileTypes( "*.mdl" )
		SMenu.frame.popup.propsPopup:SetOpen( true )
		SMenu.frame.popup.propsPopup:SetCurrentFolder( "props_badlands" )
		SMenu.frame.popup.propsPopup:SetModels( true )

		SMenu.frame.popup.searchTextEntry = vgui.Create("STextEntry", SMenu.frame.popup.props)
		SMenu.frame.popup.searchTextEntry:SetSize(ScrW() * 0.24, 50)
		SMenu.frame.popup.searchTextEntry:SetPos(25, 60)
		SMenu.frame.popup.searchTextEntry:SetPlaceholderText( SMenu.Language["Search"] )
		local searchX = SMenu.frame.popup.searchTextEntry:GetSize()
		SMenu.frame.popup.addButton = vgui.Create("SMenuButton", SMenu.frame.popup.props)
		SMenu.frame.popup.addButton:SetPos(searchX + 50, 60)
		SMenu.frame.popup.addButton:SetSize(searchX * 0.5, 50)
		SMenu.frame.popup.addButton:SetText(SMenu.Language["Add"])
		SMenu.frame.popup.addButton.HoverColor = SMenu.Color.GetColor("red")

		function SMenu.frame.popup.searchTextEntry:OnTextChanged()
			SMenu.frame.popup.propsPopup.Files:Clear()

			if string.Trim(self:GetText()) == "" then
				return
			end

			if string.len(self:GetText()) < 3 then
				return
			end

			if(util.GetModelInfo(self:GetText()).KeyValues != nil) then
				SMenu.frame.popup.addButton.HoverColor = SMenu.Color.GetColor("green")
				SMenu.frame.popup.addButton:SetEnabled(true)
			else 
				SMenu.frame.popup.addButton.HoverColor = SMenu.Color.GetColor("red")
				SMenu.frame.popup.addButton:SetEnabled(false)
			end

			local mod = search.GetResults(self:GetText())
			for _,v in pairs(mod) do
				v.icon:Remove()
				timer.Simple(0.01, function ()
					if not isstring(v.words[1]) then
						return
					elseif not string.StartWith(v.words[1], "models") then
						return
					end
					
					local icon = SMenu.frame.popup.propsPopup.Files:Add("SpawnIcon")
					icon:SetModel(v.words[1])
					icon.DoClick = function( pnl )
						if ( pnl.LastClickTime && SysTime() - pnl.LastClickTime < 0.3 ) then
							SMenu.frame.popup.propsPopup:OnDoubleClick( v.words[1], icon )
						else
							SMenu.frame.popup.propsPopup:OnSelect( v.words[1], icon )
						end
						pnl.LastClickTime = SysTime()
					end
					icon.DoRightClick = function()
						SMenu.frame.popup.propsPopup:OnRightClick( v.words[1], icon )
					end
				end)
			end
		end
	end

	function SMenu.frame.popup.propsPopup:OnSelect( path, pnl )
		callback(path, SMenu.frame.popup.props)
		SMenu.frame.popup.props:Close()
	end

	if(closeCallback != nil) then
		function SMenu.frame.popup.props:OnClose() 
			closeCallback()
		end
	end

	function SMenu.frame.popup.addButton:DoClick()
		callback(SMenu.frame.popup.searchTextEntry:GetText(), SMenu.frame.popup.props)
		SMenu.frame.popup.props:Close()
	end

end
