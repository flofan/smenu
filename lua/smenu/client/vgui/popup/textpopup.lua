SMenu = SMenu or {}
SMenu.frame.popup = SMenu.frame.popup or {}

function SMenu.frame.popup.AskText(question, callback, closeCallback)
	surface.SetFont("SMenu:Title")
	local tx, ty = surface.GetTextSize(question)

	local popup = vgui.Create("SFrame")
	popup:MakePopup()
	popup:SetTitle(question)
	popup:SetSize(math.Clamp(tx * 1.2, 500, 1000), 300 )
	popup:Center()
	function popup:Paint(w, h)
		draw.RoundedBox(0, 0, 0, w, h, SMenu.Color.GetColor("secondary"))
		draw.RoundedBox(0, 0, 0, w, 50, SMenu.Color.GetColor("black"))
	end

	function popup:Think()
		popup:MoveToFront()
	end

	if(closeCallback != nil) then
		function popup:OnClose() 
			closeCallback()
		end
	end

	local textEntry = vgui.Create("STextEntry", popup)
	textEntry:SetSize(250, 50)
	textEntry:Center()

	local buttonValidate = vgui.Create("SMenuButton", popup)
	buttonValidate:SetSize(200, 50)
	buttonValidate:SetPos(200, 225)
	buttonValidate:SetText(SMenu.Language["Validate"])
	buttonValidate.HoverColor = SMenu.Color.GetColor("red")
	buttonValidate:CenterHorizontal()
	function buttonValidate:DoClick()
		if string.Trim(textEntry:GetText()) == "" then
			return
		end
		popup:Close()
		local text = textEntry:GetText()
		callback(text, popup)
	end

	function textEntry:OnChange()
		if string.Trim(textEntry:GetText()) == "" then
			buttonValidate.HoverColor = SMenu.Color.GetColor("red")
		else
			buttonValidate.HoverColor = SMenu.Color.GetColor("green")
		end
	end
end