SMenu = SMenu or {}
SMenu.frame.popup = SMenu.frame.popup or {}

function SMenu.frame.popup.ToolgunPopup(callback, ignorelist, closeCallback)
	if ignorelist == nil then
		ignorelist = {}
	end

	surface.SetFont("SMenu:Title")

	local popup = vgui.Create("SFrame")
	popup:SetDrawOnTop(true)
	popup:MakePopup()
	popup:SetTitle(SMenu.Language["SelectTool"])
	popup:SetSize(ScrW() * 0.15, ScrH() * 0.5 )
	popup:Center()
	function popup:Paint(w, h)
		draw.RoundedBox(0, 0, 0, w, h, SMenu.Color.GetColor("secondary"))
		draw.RoundedBox(0, 0, 0, w, 50, SMenu.Color.GetColor("black"))
	end

	function popup:OnFocusChanged(gained) 
		popup:RequestFocus()
	end

	function popup:Think()
		popup:MoveToFront()
	end

	if(closeCallback != nil) then
		function popup:OnClose() 
			closeCallback()
		end
	end


	local toolgunTree = vgui.Create("STree", popup)

	toolgunTree:Dock(FILL)
	toolgunTree:DockMargin(5, 25, 5, 5)

	for a in pairs(spawnmenu.GetTools()) do
		for c,d in pairs(spawnmenu.GetTools()[a].Items) do
			local addnodes = {}
			for g, h in pairs(weapons.Get("gmod_tool").Tool) do
				if h.Category and h.Category == d.ItemName then
					if table.HasValue(ignorelist, g) or ignorelist[g] != nil then
						continue
					end
					table.insert(addnodes, {h.Name, g})
				end
			end

			if #addnodes != 0 then
				local node1 = toolgunTree:AddNode(d.ItemName)
				node1.Tool = d.ItemName
				for _, f in pairs(addnodes) do
					local node2 = node1:AddNode(f[1])
					node2.Icon:SetImage("icon16/wrench.png")
					node2.Tool = f[2]
				end
			end
		end
	end
	
	function toolgunTree:OnNodeSelected(node)
		if node:GetIcon() != "icon16/wrench.png" then
			return
		end
		callback(node.Tool, popup)
		popup:Close()
	end

	
end
