
SMenu = SMenu or {}
SMenu.Color = SMenu.Color or {}

--[[ 

	/!\ ⚠ Touch this file at your own risk ⚠ /!\

]]--

SMenu.Color["primary"] = Color(33,33,33)
	
SMenu.Color["primary_light"] = Color(222, 222, 222)

SMenu.Color["secondary"] = Color(28,26,26)
SMenu.Color["secondary_light"] = Color(178, 178, 178)

SMenu.Color["black"] = Color(18,18,18)

SMenu.Color["white"] = Color(255,255,255)

SMenu.Color["imagecolor"] = Color(255,255,255)

SMenu.Color["accent"] = Color(75,75,75)

SMenu.Color["accentdark"] = Color(50, 50, 50)

SMenu.Color["green"] = Color(29,131,72)

SMenu.Color["red"] = Color(190,71,71)