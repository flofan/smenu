surface.CreateFont("SMenu:Title", {
	font = "Circular Std Medium",
	size = 24
})

surface.CreateFont("SMenu:Text", {
	font = "Circular Std Medium",
	size = 20
})

surface.CreateFont("SMenu:Label", {
	font = "Circular Std Medium",
	size = 16
})