local PANEL = {}

function PANEL:Init()

	self:SetDraggable(false)
	self:SetSize(500, 500)
	self.lblTitle:SetFont("SMenu:Title")
	self.lblTitle:SetTextColor(SMenu.Color.GetColor("white"))
	self.lblTitle:SetPos(20, 120)
	self:ShowCloseButton(true)
	self:SetAlpha(0)
	self:FadeIn()

	self.btnMinim:SetVisible(false)
	self.btnMaxim:SetVisible(false)

	self.btnClose:SetText("✕")
	self.btnClose:SetFont("SMenu:Title")
	self.btnClose:SetTextColor(SMenu.Color.GetColor("white"))
	self.btnClose.ActualColor = SMenu.Color.GetColor("black")
	self.btnClose.Paint = function( s, w, h )
		if s:IsHovered() then
			s.ActualColor = SMenu.utils.LerpColor(8 * RealFrameTime(), s.ActualColor, SMenu.Color.GetColor("red"))
			draw.RoundedBox(0, 0, 0, w, h, s.ActualColor)
		else 
			s.ActualColor = SMenu.utils.LerpColor(8 * RealFrameTime(), s.ActualColor, SMenu.Color.GetColor("black"))
			draw.RoundedBox(0, 0, 0, w, h, s.ActualColor)
		end
	end

	self:DockPadding( 5, 50 + 5, 5, 5 )
end

function PANEL:Close()

	self:AlphaTo(0, 0.2, 0, function() 
		self:SetVisible( false )

		if ( self:GetDeleteOnClose() ) then
			self:Remove()
		else 
			self:Hide()
		end

		self:OnClose()
	
	end)
end

function PANEL:FadeIn()

	self:AlphaTo(255, 0.2, 0)

end

function PANEL:Paint(w, h)

	draw.RoundedBox(0, 0, 0, w, h, SMenu.Color.GetColor("primary"))
	draw.RoundedBox(0, 0, 0, w, 50, SMenu.Color.GetColor("black"))

end

function PANEL:PerformLayout()

	local titlePush = 0

	if ( IsValid( self.imgIcon ) ) then

		self.imgIcon:SetPos( 5, 5 )
		self.imgIcon:SetSize( 16, 16 )
		titlePush = 16

	end

	self.btnClose:SetPos( self:GetWide() - 50, 0 )
	self.btnClose:SetSize( 50, 50 )

	self.btnMaxim:SetPos( self:GetWide() - 31 * 2 - 4, 0 )
	self.btnMaxim:SetSize( 31, 24 )

	self.btnMinim:SetPos( self:GetWide() - 31 * 3 - 4, 0 )
	self.btnMinim:SetSize( 31, 24 )

	self.lblTitle:SetPos( 15, 15 )
	self.lblTitle:SetSize( self:GetWide() - 25 - titlePush, 20 )

end

vgui.Register("SFrame", PANEL, "DFrame")