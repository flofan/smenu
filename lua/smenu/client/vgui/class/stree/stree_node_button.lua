local PANEL = {}

function PANEL:Init()

	self:SetTextInset( 50, 0 )
	self:SetContentAlignment( 4 )
	self:SetFont("SMenu:Text")

end

function PANEL:Paint( w, h )
	return false
end

function PANEL:UpdateColours( skin )
	if ( self:IsSelected() ) then return self:SetTextStyleColor( SMenu.Color.GetColor("accent") ) end
	if ( self.Hovered ) then return self:SetTextStyleColor( SMenu.Color.GetColor("accent") ) end
	return self:SetTextStyleColor( SMenu.Color.GetColor("white") )
end

function PANEL:GenerateExample()

end

derma.DefineControl( "STree_Node_Button", "Tree Node Button", PANEL, "SMenuButton" )