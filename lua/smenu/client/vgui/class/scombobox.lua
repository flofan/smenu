local PANEL = {}

function PANEL:Init()

	self:SetFont("SMenu:Title")
	self:SetTextColor(SMenu.Color.GetColor("white"))
	self:SetContentAlignment(5)

end	

function PANEL:OpenMenu( pControlOpener )

	if ( pControlOpener && pControlOpener == self.TextEntry ) then
		return
	end
	if ( #self.Choices == 0 ) then return end
	if ( IsValid( self.Menu ) ) then
		self.Menu:Remove()
		self.Menu = nil
	end

	self.Menu = DermaMenu( false, self )

	if ( self:GetSortItems() ) then
		local sorted = {}
		for k, v in pairs( self.Choices ) do
			local val = tostring( v )
			if ( string.len( val ) > 1 && !tonumber( val ) && val:StartWith( "#" ) ) then val = language.GetPhrase( val:sub( 2 ) ) end
			table.insert( sorted, { id = k, data = v, label = val } )
		end
		for k, v in SortedPairsByMemberValue( sorted, "label" ) do
			local option = self.Menu:AddOption( v.data, function() self:ChooseOption( v.data, v.id ) end )
			if ( self.ChoiceIcons[ v.id ] ) then
				option:SetIcon( self.ChoiceIcons[ v.id ] )
			end
		end
	else
		for k, v in pairs( self.Choices ) do
			local option = self.Menu:AddOption( v, function() self:ChooseOption( v, k ) end )
			if ( self.ChoiceIcons[ k ] ) then
				option:SetIcon( self.ChoiceIcons[ k ] )
			end
		end
	end

	local x, y = self:LocalToScreen( 0, self:GetTall() )

	self.Menu:SetMinimumWidth( self:GetWide() )
	self.Menu:Open( x, y, false, self )

	for _,v in pairs(self.Menu:GetCanvas():GetChildren()) do
		v:SetTextColor(SMenu.Color.GetColor("white"))
		v.ActualColor = SMenu.Color.GetColor("black")
		v:SetFont("SMenu:Text")
		v.Paint = function(s, w, h)
			if s.Depressed then
				s.ActualColor = SMenu.utils.LerpColor(16 * RealFrameTime(), s.ActualColor, SMenu.Color.GetColor("black"))
				draw.RoundedBox(0, 0, 0, w, h, s.ActualColor)
				return
			end
		
			if s:IsHovered() then
				s.ActualColor = SMenu.utils.LerpColor(8 * RealFrameTime(), s.ActualColor, SMenu.Color.GetColor("accent"))
				draw.RoundedBox(0, 0, 0, w, h, s.ActualColor)
			else 
				s.ActualColor = SMenu.utils.LerpColor(8 * RealFrameTime(), s.ActualColor, SMenu.Color.GetColor("black"))
				draw.RoundedBox(0, 0, 0, w, h, s.ActualColor)
			end
			if s:GetToggle() then
				draw.RoundedBox(0, 0, 0, w, h, SMenu.Color.GetColor("accent"))
			end
		end
	end

end

function PANEL:GetDataByChoice( choice )

	for id, dat in pairs( self.Choices ) do
		if ( dat == choice ) then
			return self.Data[ id ]
		end
	end

end

function PANEL:SetDataChoice( choice, data )

	for id, dat in pairs( self.Choices ) do
		if ( dat == choice ) then
			self.Data[ id ] = data
		end
	end

end

function PANEL:Paint(w, h)

	draw.RoundedBox(0, 0, 0, w, h, SMenu.Color.GetColor("black"))

end	

vgui.Register("SComboBox", PANEL,"DComboBox")