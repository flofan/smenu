local PANEL = {}

function PANEL:Init()

	self:SetSize(100, 25)
	self:SetTextColor(SMenu.Color.GetColor("white"))
	self:SetFont("SMenu:Title")

	self:SetTextColor( SMenu.Color.GetColor("white") )
	self:SetHighlightColor(SMenu.Color.GetColor("accent"))
	self:SetCursorColor(SMenu.Color.GetColor("white"))

end

function PANEL:Paint(w,h)

	
	draw.RoundedBox(0, 0, 0, w, h, SMenu.Color.GetColor("black"))

	if ( self.GetPlaceholderText && self.GetPlaceholderColor && self:GetPlaceholderText() && self:GetPlaceholderText():Trim() != "" && self:GetPlaceholderColor() && ( !self:GetText() || self:GetText() == "" ) ) then

		local oldText = self:GetText()
	
		local str = self:GetPlaceholderText()
		if ( str:StartWith( "#" ) ) then str = str:sub( 2 ) end
		str = language.GetPhrase( str )
	
		self:SetText( str )
		self:DrawTextEntryText( self:GetPlaceholderColor(), self:GetHighlightColor(), self:GetCursorColor() )
		self:SetText( oldText )
	
		return
	end
	
	self:DrawTextEntryText( self:GetTextColor(), self:GetHighlightColor(), self:GetCursorColor() )
end

vgui.Register("STextEntry", PANEL, "DTextEntry")