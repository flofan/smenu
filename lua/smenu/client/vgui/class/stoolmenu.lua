local PANEL = {}
function PANEL:Init()

	self.ToolPanels = {}

	self:SetFadeTime( 0 )

end

function PANEL:LoadTools()

	local tools = spawnmenu.GetTools()

	self:AddToolPanel( 1, tools[1] )

end

function PANEL:AddToolPanel( Name, ToolTable )

	local Panel = vgui.Create( "SToolPanel" )
	Panel:SetTabID( Name )
	Panel:LoadToolsFromTable( ToolTable.Items )

	if(ScrH() >= 1080) then
		self:AddSheet( ToolTable.Label, Panel, "smenu/toolgun.png" )
	else
		self:AddSheet( ToolTable.Label, Panel )
	end

	self.ToolPanels[ Name ] = Panel

end

function PANEL:Paint( w, h )
end

function PANEL:GetToolPanel( id )

	return self.ToolPanels[ id ]

end

vgui.Register( "SToolMenu", PANEL, "SPropertySheet" )