SMenu = SMenu or {}
SMenu.frame = SMenu.frame or {}
SMenu.frame.config_mainframe = SMenu.frame.config_mainframe or {}

local scrw, scrh = ScrW(), ScrH()

function SMenu.frame.config_mainframe.Refresh()
	-- if IsValid(SMenu.frame.config_mainframe.frame) then
	-- 	SMenu.frame.config_category.RefreshValue()
	-- 	SMenu.frame.config_tab.RefreshValue()
	-- 	SMenu.frame.config_tool.RefreshValue()
	-- 	SMenu.frame.config_category.RefreshComboBox()
	-- 	SMenu.frame.config_tab.RefreshComboBox()
	-- 	SMenu.frame.config_tool.RefreshComboBox()
	-- end
end

function SMenu.frame.config_mainframe.Show()

	SMenu.frame.config_mainframe.frame = vgui.Create("SFrame")
	SMenu.frame.config_mainframe.frame:SetSize(scrw * 0.47, scrh * 0.5)
	SMenu.frame.config_mainframe.frame:Center()
	SMenu.frame.config_mainframe.frame:MakePopup()
	SMenu.frame.config_mainframe.frame:SetTitle(SMenu.Language["Parameters"])

	local cmw, cmh = SMenu.frame.config_mainframe.frame:GetSize()

	SMenu.frame.config_mainframe.categorylist = vgui.Create("SCategoryList", SMenu.frame.config_mainframe.frame)
	local cat = SMenu.frame.config_mainframe.categorylist:Add("")
	cat.Header:SetSize(0,0)
	local button_category = cat:Add(SMenu.Language["Category"])
	button_category:SetTall(70)
	button_category:SetContentAlignment(5)
	button_category:SetTextInset(0, 0)
	function button_category:DoClick()
		if not SMenu.frame.config_category.IsOpen() then
			SMenu.frame.config_mainframe.horizontal_divider:SetRight(SMenu.frame.config_category.Show())
		end
		SMenu.frame.config_tab.Close()
		SMenu.frame.config_color.Close()
		SMenu.frame.config_tool.Close()
	end

	local button_tabs = cat:Add(SMenu.Language["Tab"])
	button_tabs:SetTall(70)
	button_tabs:SetContentAlignment(5)
	button_tabs:SetTextInset(0, 0)
	function button_tabs:DoClick()
		if not SMenu.frame.config_tab.IsOpen() then
			SMenu.frame.config_mainframe.horizontal_divider:SetRight(SMenu.frame.config_tab.Show())
		end
		SMenu.frame.config_category.Close()
		SMenu.frame.config_color.Close()
		SMenu.frame.config_tool.Close()
	end

	local button_color = cat:Add(SMenu.Language["Colors"])
	button_color:SetTall(70)
	button_color:SetContentAlignment(5)
	button_color:SetTextInset(0, 0)
	function button_color:DoClick()
		if not SMenu.frame.config_color.IsOpen() then
			SMenu.frame.config_mainframe.horizontal_divider:SetRight(SMenu.frame.config_color.Show())
		end
		SMenu.frame.config_category.Close()
		SMenu.frame.config_tab.Close()
		SMenu.frame.config_tool.Close()
	end

	local button_tools = cat:Add(SMenu.Language["Tool"])
	button_tools:SetTall(70)
	button_tools:SetContentAlignment(5)
	button_tools:SetTextInset(0, 0)
	function button_tools:DoClick()
		
		if not SMenu.frame.config_tool.IsOpen() then
			SMenu.frame.config_mainframe.horizontal_divider:SetRight(SMenu.frame.config_tool.Show())
		end

		SMenu.frame.config_category.Close()
		SMenu.frame.config_tab.Close()
		SMenu.frame.config_color.Close()
	end

	function SMenu.frame.config_mainframe.categorylist:Paint(w, h)
		draw.RoundedBox(0, 0, 0, w, h, SMenu.Color.GetColor("secondary"))
	end
	
	SMenu.frame.config_mainframe.horizontal_divider = vgui.Create("DHorizontalDivider", SMenu.frame.config_mainframe.frame)
	SMenu.frame.config_mainframe.horizontal_divider:Dock(FILL)
	SMenu.frame.config_mainframe.horizontal_divider:DockMargin(-5, -5, -5, -5)
	SMenu.frame.config_mainframe.horizontal_divider:SetLeft(SMenu.frame.config_mainframe.categorylist)
	SMenu.frame.config_mainframe.horizontal_divider:SetLeftMin(cmw * 0.2)
	SMenu.frame.config_mainframe.horizontal_divider:SetRightMin(cmw * 0.5)
	SMenu.frame.config_mainframe.horizontal_divider:SetLeftWidth(cmw * 0.3)
end