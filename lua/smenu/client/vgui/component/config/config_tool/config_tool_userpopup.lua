SMenu = SMenu or {}
SMenu.frame = SMenu.frame or {}
SMenu.frame.config_tool = SMenu.frame.config_tool or {}
SMenu.frame.config_tool.tool = SMenu.frame.config_tool.tool or {}

function SMenu.frame.config_tool.showUsergroup(closeCallback)

	local popup = vgui.Create("SFrame")
	popup:SetTitle(SMenu.Language["UserGroup"])
	popup:Center()
	popup:MakePopup()

	local sizeW, sizeH = popup:GetSize()

	local listView = vgui.Create("SListView", popup)
	listView:SetPos(sizeW * 0.1, sizeH * 0.12)
	listView:SetSize(sizeW * 0.5, sizeH * 0.7)
	listView:AddColumn(SMenu.Language["Sort"])

	for _, v in pairs(SMenu.frame.config_tool.tool) do
		listView:AddLine(v)
	end

	local saveButton = vgui.Create("SMenuButton", popup)
	saveButton.HoverColor = SMenu.Color.GetColor("green")
	saveButton:SetPos(sizeW * 0.1, sizeH * 0.85)
	saveButton:SetSize(sizeW * 0.82, sizeH * 0.1)
	saveButton:SetText(SMenu.Language["Save"])

	function saveButton.DoClick()
		popup:IsKeyboardInputEnabled(false)
		popup:IsMouseInputEnabled(false)

		SMenu.frame.config_tool.tool = {}

		for _,v in pairs(listView:GetLines()) do
			table.insert(SMenu.frame.config_tool.tool, v:GetColumnText(1))
		end

		SMenu.frame.config.toolgun.ModifyToolgun(SMenu.frame.config_tool.toolname, SMenu.frame.config_tool.tool)

		SMenu.frame.popup.Info( SMenu.Language["SuccessSave"] ,function () 
			popup:IsKeyboardInputEnabled(true)
			popup:IsMouseInputEnabled(true)
		end)

		SMenu.frame.config_tool.RefreshValue()
	end

	local addButton = vgui.Create("SMenuButton", popup)
	addButton.HoverColor = SMenu.Color.GetColor("green")
	addButton:SetPos(sizeW * 0.62, sizeH * 0.12)
	addButton:SetSize(sizeW * 0.3, sizeH * 0.1)
	addButton:SetText(SMenu.Language["Add"])

	function addButton.DoClick()
		popup:IsKeyboardInputEnabled(false)
		popup:IsMouseInputEnabled(false)

		SMenu.frame.popup.AskText(SMenu.Language["AskUserGroup"], function(res)

			for _, v in pairs(listView:GetLines()) do
				if(v:GetColumnText(1) == res) then
					SMenu.frame.popup.Info(SMenu.Language["AlreadyExist"], closeCallback)
					popup:IsKeyboardInputEnabled(true)
					popup:IsMouseInputEnabled(true)
					return
				end
			end

			listView:AddLine(res)
			popup:IsKeyboardInputEnabled(true)
			popup:IsMouseInputEnabled(true)
		end, function () 
			popup:IsKeyboardInputEnabled(true)
			popup:IsMouseInputEnabled(true)
		end)
	end

	local removeButton = vgui.Create("SMenuButton", popup)
	removeButton.HoverColor = SMenu.Color.GetColor("red")
	removeButton:SetPos(sizeW * 0.62, sizeH * 0.24)
	removeButton:SetSize(sizeW * 0.3, sizeH * 0.1)
	removeButton:SetText(SMenu.Language["Remove"])

	function removeButton.DoClick()
		for k,v in pairs(listView:GetLines()) do
			if(table.HasValue(listView:GetSelected(), v)) then
				listView:RemoveLine(k)
			end
		end
	end

	if(closeCallback != nil) then
		function popup:OnClose() 
			closeCallback()
		end
	end
end