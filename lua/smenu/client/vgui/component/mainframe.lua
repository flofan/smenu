SMenu = SMenu or {}
SMenu.frame = SMenu.frame or {}
SMenu.frameFunc = SMenu.frameFunc or {}

SMenu.frame.main_panel = nil

local ScreenW, ScreenH = ScrW(), ScrH()

function SMenu.frameFunc.main()

	SMenu.frame.main_panel = vgui.Create("DPanel")
	SMenu.frame.main_panel:SetSize(ScreenW * 0.7, ScreenH * 0.9)
	SMenu.frame.main_panel:CenterHorizontal(0.37)
	SMenu.frame.main_panel:CenterVertical()
	SMenu.frame.main_panel:SetAlpha(0)
	SMenu.frame.main_panel:AlphaTo(255, 0.1)
	function SMenu.frame.main_panel.Paint(s,w,h)
		draw.RoundedBox(0, 0, 0, w, h, SMenu.Color.GetColor("primary"))
	end

	SMenu.frameFunc.tool()
	SMenu.frameFunc.tabs()
end

function SMenu.frameFunc.setTab(type)

	SMenu.frame.main_panel.tabSelected = type

	if type == "props" then
		SMenu.frameFunc.props.Show()
		SMenu.frameFunc.vehicles.Close()
		SMenu.frameFunc.weapons.Close()
		SMenu.frameFunc.entities.Close()
		if IsValid(SMenu.frame.tabs_button_props) then
			SMenu.frame.tabs_button_props:SetToggle(true)
		end
		if IsValid(SMenu.frame.tabs_button_weapons) then
			SMenu.frame.tabs_button_weapons:SetToggle(false)
		end
		if IsValid(SMenu.frame.tabs_button_entities) then
			SMenu.frame.tabs_button_entities:SetToggle(false)
		end
		if IsValid(SMenu.frame.tabs_button_vehicles) then
			SMenu.frame.tabs_button_vehicles:SetToggle(false)
		end
	elseif type == "entities" then
		SMenu.frameFunc.props.Close()
		SMenu.frameFunc.vehicles.Close()
		SMenu.frameFunc.weapons.Close()
		SMenu.frameFunc.entities.Show()
		if IsValid(SMenu.frame.tabs_button_weapons) then
			SMenu.frame.tabs_button_weapons:SetToggle(false)
		end
		if IsValid(SMenu.frame.tabs_button_props) then
			SMenu.frame.tabs_button_props:SetToggle(false)
		end
		if IsValid(SMenu.frame.tabs_button_entities) then
			SMenu.frame.tabs_button_entities:SetToggle(true)
		end
		if IsValid(SMenu.frame.tabs_button_vehicles) then
			SMenu.frame.tabs_button_vehicles:SetToggle(false)
		end
	elseif type == "weapons" then
		SMenu.frameFunc.props.Close()
		SMenu.frameFunc.vehicles.Close()
		SMenu.frameFunc.entities.Close()
		SMenu.frameFunc.weapons.Show()
		if IsValid(SMenu.frame.tabs_button_weapons) then
			SMenu.frame.tabs_button_weapons:SetToggle(true)
		end
		if IsValid(SMenu.frame.tabs_button_props) then
			SMenu.frame.tabs_button_props:SetToggle(false)
		end
		if IsValid(SMenu.frame.tabs_button_entities) then
			SMenu.frame.tabs_button_entities:SetToggle(false)
		end
		if IsValid(SMenu.frame.tabs_button_vehicles) then
			SMenu.frame.tabs_button_vehicles:SetToggle(false)
		end
	elseif type == "vehicles" then
		SMenu.frameFunc.vehicles.Show()
		SMenu.frameFunc.props.Close()
		SMenu.frameFunc.weapons.Close()
		SMenu.frameFunc.entities.Close()
		if IsValid(SMenu.frame.tabs_button_weapons) then
			SMenu.frame.tabs_button_weapons:SetToggle(false)
		end
		if IsValid(SMenu.frame.tabs_button_props) then
			SMenu.frame.tabs_button_props:SetToggle(false)
		end
		if IsValid(SMenu.frame.tabs_button_entities) then
			SMenu.frame.tabs_button_entities:SetToggle(false)
		end
		if IsValid(SMenu.frame.tabs_button_vehicles) then
			SMenu.frame.tabs_button_vehicles:SetToggle(true)
		end
	elseif type == "nothing" then
		SMenu.frameFunc.vehicles.Close()
		SMenu.frameFunc.props.Close()
		SMenu.frameFunc.weapons.Close()
		SMenu.frameFunc.entities.Close()
	end
end

function SMenu.frameFunc.OpenMenu()

	if not IsValid(SMenu.frame.main_panel) then 
		SMenu.frameFunc.main()
	end
	SMenu.frame.main_panel:Show()
	SMenu.frame.main_panel:MakePopup()
end

function SMenu.frameFunc.CloseMenu()
	SMenu.frame.tool_panel.Close()
	SMenu.frame.dontClose = false
	SMenu.frame.main_panel:AlphaTo(0, 0.1, 0, function() 
		for k,v in pairs(SMenu.frame) do
			if(isstring(v)) then
				continue
			end

			if IsValid(v) then
				v:Remove()
			end
		end
	end)
end