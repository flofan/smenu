SMenu = SMenu or {}
SMenu.frame = SMenu.frame or {}
SMenu.frameFunc = SMenu.frameFunc or {}
SMenu.frame.tabs_lastTabSelect = "props"

function SMenu.frameFunc.tabs()
	SMenu.frame.config.tabs.Reload()
	local ParW, ParH = SMenu.frame.main_panel:GetSize()
	SMenu.frame.tabs = vgui.Create("DPanel", SMenu.frame.main_panel)
	SMenu.frame.tabs:SetSize(ParW, ParH * 0.075)
	function SMenu.frame.tabs.Paint(s,w,h)
		draw.RoundedBox(0, 0, 0, w, h, SMenu.Color.GetColor("black"))
	end

	local TabsW, TabsH = SMenu.frame.tabs:GetSize()
	local ScrH = ScrH()
	local pos = 0.058

	SMenu.frame.tabs_button_props = vgui.Create("SMenuButton", SMenu.frame.tabs)
	SMenu.frame.tabs_button_props:SetSize(TabsW * 0.058, TabsH)
	SMenu.frame.tabs_button_props.HoverColor = SMenu.Color.GetColor("red")
	SMenu.frame.tabs_button_props:SetText("✕")
	function SMenu.frame.tabs_button_props:DoClick()
		SMenu.frameFunc.CloseMenu()
	end

	if SMenu.frame.config.tabs.isAllowedToSeeTab(LocalPlayer(), "props") then
		SMenu.frame.tabs_button_props = vgui.Create("SMenuButton", SMenu.frame.tabs)
		SMenu.frame.tabs_button_props:SetSize(TabsW * 0.2 + 1, TabsH)
		SMenu.frame.tabs_button_props:SetPos(TabsW * pos, 0)
		SMenu.frame.tabs_button_props.HoverColor = SMenu.Color.GetColor("accentdark")
		if(ScrH >= 1080) then
			SMenu.frame.tabs_button_props:SetImage("smenu/props.png")

			if(SMenu.Color.IsLightTheme()) then
				SMenu.frame.tabs_button_props.m_Image:SetImageColor(SMenu.Color["secondary_light"])
			end
		end
		SMenu.frame.tabs_button_props:SetText(SMenu.Language["Props"])
		function SMenu.frame.tabs_button_props:DoClick()
			SMenu.frameFunc.setTab("props")
			SMenu.frame.tabs_lastTabSelect = "props"
		end
		pos = pos + 0.2
	end

	if SMenu.frame.config.tabs.isAllowedToSeeTab(LocalPlayer(), "weapons") then
		SMenu.frame.tabs_button_weapons = vgui.Create("SMenuButton", SMenu.frame.tabs)
		SMenu.frame.tabs_button_weapons:SetSize(TabsW * 0.2 + 1, TabsH)
		SMenu.frame.tabs_button_weapons:SetPos(TabsW * pos, 0)
		SMenu.frame.tabs_button_weapons:SetIsToggle(true)
		SMenu.frame.tabs_button_weapons.HoverColor = SMenu.Color.GetColor("accentdark")
		if(ScrH >= 1080) then
			SMenu.frame.tabs_button_weapons:SetImage("smenu/weapons.png")

			if(SMenu.Color.IsLightTheme()) then
				SMenu.frame.tabs_button_weapons.m_Image:SetImageColor(SMenu.Color["secondary_light"])
			end
		end
		SMenu.frame.tabs_button_weapons:SetText(SMenu.Language["Weapons"])
		function SMenu.frame.tabs_button_weapons:DoClick()
			SMenu.frameFunc.setTab("weapons")
			SMenu.frame.tabs_lastTabSelect = "weapons"
		end
		pos = pos + 0.2
	end

	if SMenu.frame.config.tabs.isAllowedToSeeTab(LocalPlayer(), "entities") then
		SMenu.frame.tabs_button_entities = vgui.Create("SMenuButton", SMenu.frame.tabs) 
		SMenu.frame.tabs_button_entities:SetSize(TabsW * 0.2 + 1, TabsH)
		SMenu.frame.tabs_button_entities:SetPos(TabsW * pos, 0)
		SMenu.frame.tabs_button_entities.HoverColor = SMenu.Color.GetColor("accentdark")
		SMenu.frame.tabs_button_entities:SetIsToggle(true)
		if(ScrH >= 1080) then
			SMenu.frame.tabs_button_entities:SetImage("smenu/entities.png")
			if(SMenu.Color.IsLightTheme()) then
				SMenu.frame.tabs_button_entities.m_Image:SetImageColor(SMenu.Color["secondary_light"])
			end
		end
		SMenu.frame.tabs_button_entities:SetText(SMenu.Language["Entities"])
		function SMenu.frame.tabs_button_entities:DoClick()
			SMenu.frameFunc.setTab("entities")
			SMenu.frame.tabs_lastTabSelect = "entities"
		end
		pos = pos + 0.2
	end

	if SMenu.frame.config.tabs.isAllowedToSeeTab(LocalPlayer(), "vehicles") then
		SMenu.frame.tabs_button_vehicles = vgui.Create("SMenuButton", SMenu.frame.tabs) 
		SMenu.frame.tabs_button_vehicles:SetSize(TabsW * 0.2, TabsH)
		SMenu.frame.tabs_button_vehicles:SetPos(TabsW * pos, 0)
		SMenu.frame.tabs_button_vehicles.HoverColor = SMenu.Color.GetColor("accentdark")
		SMenu.frame.tabs_button_vehicles:SetIsToggle(true)
		if(ScrH >= 1080) then
			SMenu.frame.tabs_button_vehicles:SetImage("smenu/vehicles.png")

			if(SMenu.Color.IsLightTheme()) then
				SMenu.frame.tabs_button_vehicles.m_Image:SetImageColor(SMenu.Color["secondary_light"])
			end
		end
		SMenu.frame.tabs_button_vehicles:SetText(SMenu.Language["Vehicles"])
		function SMenu.frame.tabs_button_vehicles:DoClick()
			SMenu.frameFunc.setTab("vehicles")
			SMenu.frame.tabs_lastTabSelect = "vehicles"
		end
		pos = pos + 0.2
	end

	if table.HasValue(SMenu.Config.OldMenu, LocalPlayer():GetUserGroup()) then
		SMenu.frame.tabs_open_oldmenu_button = vgui.Create("SMenuButton", SMenu.frame.tabs) 
		SMenu.frame.tabs_open_oldmenu_button:SetSize(TabsW * 0.058, TabsH)
		SMenu.frame.tabs_open_oldmenu_button:SetPos(TabsW - TabsW * 0.054 * 2 + 1, 0)
		SMenu.frame.tabs_open_oldmenu_button:SetText("")
		if(ScrH >= 1080) then
			SMenu.frame.tabs_open_oldmenu_button:SetImage("smenu/oldmenu.png")
		else
			SMenu.frame.tabs_open_oldmenu_button:SetImage("smenu/oldmenu_32.png")
		end
		if(SMenu.Color.IsLightTheme()) then
			SMenu.frame.tabs_open_oldmenu_button.m_Image:SetImageColor(SMenu.Color["secondary_light"])
		end

		function SMenu.frame.tabs_open_oldmenu_button:DoClick()
			SMenu.frameFunc.CloseMenu()
			SMenu.frameFunc.OpenOldMenu()
		end
	end

	if table.HasValue(SMenu.Config.AllowedGroup, LocalPlayer():GetUserGroup()) then
		SMenu.frame.tabs_button_options = vgui.Create("SMenuButton", SMenu.frame.tabs) 
		SMenu.frame.tabs_button_options:SetSize(TabsW * 0.054, TabsH)
		SMenu.frame.tabs_button_options:SetPos(TabsW - TabsW * 0.054 + 1, 0)

		SMenu.frame.tabs_button_options:SetText("")
		if(ScrH >= 1080) then
			SMenu.frame.tabs_button_options:SetImage("smenu/options.png")
		else
			SMenu.frame.tabs_button_options:SetImage("smenu/options_32.png")
		end
		if(SMenu.Color.IsLightTheme()) then
			SMenu.frame.tabs_button_options.m_Image:SetImageColor(SMenu.Color["secondary_light"])
		end
		SMenu.frame.tabs_button_options:SetContentAlignment(5)
		function SMenu.frame.tabs_button_options:DoClick()
			SMenu.frameFunc.CloseMenu()
			SMenu.frame.config_mainframe.Show()
		end
	end

	if (!SMenu.frame.config.tabs.isAllowedToSeeTab(LocalPlayer(), SMenu.frame.tabs_lastTabSelect)) then
		if(SMenu.frame.config.tabs.isAllowedToSeeTab(LocalPlayer(), "props")) then
			SMenu.frame.tabs_lastTabSelect = "props"
		elseif (SMenu.frame.config.tabs.isAllowedToSeeTab(LocalPlayer(), "vehicles")) then
			SMenu.frame.tabs_lastTabSelect = "vehicles"
		elseif (SMenu.frame.config.tabs.isAllowedToSeeTab(LocalPlayer(), "weapons")) then
			SMenu.frame.tabs_lastTabSelect = "weapons"
		elseif (SMenu.frame.config.tabs.isAllowedToSeeTab(LocalPlayer(), "entities")) then
			SMenu.frame.tabs_lastTabSelect = "entities"
		else 
			SMenu.frame.tabs_lastTabSelect = "nothing"
		end
	end


	SMenu.frameFunc.setTab(SMenu.frame.tabs_lastTabSelect)
end