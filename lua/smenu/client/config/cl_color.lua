SMenu.Color = SMenu.Color or {}
SMenu.Color.Config = SMenu.Color.Config or {}
SMenu.Color.CalculatedColor = SMenu.Color.CalculatedColor or {}

function SMenu.Color.GetColor(name)
	return SMenu.Color.CalculatedColor[name]
end

function SMenu.Color.IsLightTheme()

	return SMenu.Color.Config["light_theme"]

end 

function SMenu.Color.SetAccentColor(r, g, b)
	SMenu.Color.Config["color"] = {r, g, b}
end

function SMenu.Color.SetIsLightTheme(bool)
	SMenu.Color.Config["light_theme"] = bool
end

function SMenu.Color.GetAccentColor()
	local accentArray = SMenu.Color.Config["color"]
	return Color(accentArray[1], accentArray[2], accentArray[3])
end

function SMenu.Color.SendToGlobalConfig()
	SMenu.ConfigManager.ModifyConfig("color", SMenu.Color.Config)
end

function SMenu.Color.LoadConfig()
	SMenu.Color.Config = SMenu.ConfigManager.GetConfig("color")
end

function SMenu.Color.LoadColors()
	for k,v in pairs(SMenu.Color) do
		if(!istable(v)) then
			continue
		end

		if(k == "accent") then
			SMenu.Color.CalculatedColor["accent"] = SMenu.Color.AdditionColor(SMenu.Color["accent"],  SMenu.Color.GetAccentColor())
			continue
		elseif(k == "accentdark") then
			SMenu.Color.CalculatedColor["accentdark"] = SMenu.Color.AdditionColor(SMenu.Color["accentdark"],  SMenu.Color.GetAccentColor())
			continue
		elseif(k == "black") then
			if(SMenu.Color.IsLightTheme()) then
				SMenu.Color.CalculatedColor["black"] = SMenu.Color["white"]
				continue
			end
		elseif(k == "white") then
			if(SMenu.Color.IsLightTheme()) then
				SMenu.Color.CalculatedColor["white"] = SMenu.Color["black"]
				continue
			end
		elseif(k == "secondary") then
			if(SMenu.Color.IsLightTheme()) then
				SMenu.Color.CalculatedColor["secondary"] = SMenu.Color["secondary_light"]
				continue
			end
		elseif(k == "primary") then
			if(SMenu.Color.IsLightTheme()) then
				SMenu.Color.CalculatedColor["primary"] = SMenu.Color["primary_light"]
				continue
			end
		end

		SMenu.Color.CalculatedColor[k] = SMenu.Color[k]
	end
end

function SMenu.Color.AdditionColor(colora, colorb)
	local color = Color(0,0,0)
	color.r = (0.5*colora.r + 0.5*colorb.r)
	color.g = (0.5*colora.g + 0.5*colorb.g)
	color.b = (0.5*colora.b + 0.5*colorb.b)
	return color
end