SMenu = SMenu or {}
SMenu.ConfigManager = SMenu.ConfigManager or {}
SMenu.ConfigManager.Configs = SMenu.ConfigManager.Configs or {}

function SMenu.ConfigManager.Reload()
	local tableParam = {action = "get"}
	local tableJson = util.TableToJSON(tableParam)
	local compressed = util.Compress(tableJson)
	net.Start("SMenu:Config")
	net.WriteData(compressed, #compressed)
	net.SendToServer()
end

function SMenu.ConfigManager.ModifyConfig(id, value)
	SMenu.ConfigManager.Configs[id].value = value
	local tableParam = {action = "set", id = id, value = value}
	local tableJson = util.TableToJSON(tableParam)
	local compressed = util.Compress(tableJson)
	net.Start("SMenu:Config")
	net.WriteData(compressed, #compressed)
	net.SendToServer()
end

function SMenu.ConfigManager.GetConfig(id)
	return SMenu.ConfigManager.Configs[id].value
end

net.Receive("SMenu:Config", function(len)
	local compressTable = net.ReadData(len/8)
	local tableJson = util.Decompress(compressTable)
	local rtable = util.JSONToTable(tableJson)
	local type = rtable.action
	if type == "reload" then
		SMenu.ConfigManager.Configs = rtable.config
		SMenu.frame.config.categoryconfig.Reload()
		SMenu.frame.config.toolgun.Reload()
		SMenu.Color.LoadConfig()
		SMenu.Color.LoadColors()
		SMenu.frame.config_mainframe.Refresh()
	end
end)