AddCSLuaFile()

SMenu = SMenu or {}
SMenu.Language = {
	["Props"] = "Props",
	["Entities"] = "Entities",
	["Vehicles"] = "Vehicles",
	["Tools"] = "Tools",
	["Weapons"] = "Weapons",
	["Parameters"] = "Settings",
	["CategoryQuestion"] = "What's the category's name?",
	["Category"] = "Categories",
	["Validate"] = "Validate",
	["Delete"] = "Delete",
	["Save"] = "Save",
	["SelectCat"] = "Select a category",
	["Add"] = "Add",
	["Remove"] = "Remove",
	["UserGroup"] = "User Groups",
	["Sort"] = "Sort",
	["AskUserGroup"] = "Type the usergroup",
	["AskProp"] = "Select a prop",
	["Ok"] = "Ok",
	["SuccessSave"] = "Your change has been saved.",
	["Information"] = "Information",
	["Yes"] = "Yes",
	["No"] = "No",
	["Confirm"] = "Confirm",
	["ConfirmDelete"] = "Are you sure you want to delete this?",
	["ConfirmPaste"] = "Are you sure you want to paste this? Everything will be overwritten!",
	["Copy"] = "Copy",
	["Paste"] = "Paste",
	["PropsNotAllowed"] = "You're not allowed to spawn this props.",
	["Others"] = "Others",
	["Tool"] = "Tool Gun",
	["Tab"] = "Tabs",
	["Search"] = "Search",
	["SelectTool"] = "Select a tool",
	["AlreadyExist"] = "This is already existing",
	["PanelDoesntExist"] = "Error, the configuration panel has been closed!",
	["Colors"] = "Colors",
	["Light"] = "Light",
	["Dark"] = "Dark",
	["Theme"] = "Themes",
	["Accentuation"] = "Accentuation Color",
	["EditProps"] = "Edit Props",
	["EditUserGroups"] = "Edit UserGroups",
	["NumberProps"] = "There is %s props in this category.",
	["NumberUserGroupCategory"] = "There is %s usergroups who can see this category.",
	["NumberUserGroupTab"] = "There is %s usergroups who can see this tab.",
	["NumberUserTool"] = "There is %s usergroups who can use this tool.",
	["MoveDown"] = "Move Down",
	["MoveUp"] = "Move Up",
}
