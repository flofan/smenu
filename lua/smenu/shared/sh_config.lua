SMenu = SMenu or {}
SMenu.Config = SMenu.Config or {}

--[[-----------------------
	SMenu Configuration

Version : {{ script_version_name }}

-----------------------]]--

--[[---------------
	General
---------------]]--

-- Select the SMenu language (fr_FR, en_US, es_ES, ru_RU, tr_TR, pl_PL, ch_CHN, de_DE)
SMenu.Config.Language = "en_US"

-- Select the usergroups who are allowed to accesss to the config menu
SMenu.Config.AllowedGroup = {"superadmin", "owner"}

-- Select the usergroups who are allowed to accesss to old menu
SMenu.Config.OldMenu = {"superadmin", "owner"}

-- If a prop is not in his menu, he won't be able to spawn it
SMenu.Config.PropVerification = true

-- Select the usergroups who are allowed to spawn any props
SMenu.Config.IgnorePropVerification = {"superadmin", "owner"}

-- If a tool is not in his menu, he won't be able to use it
SMenu.Config.ToolVerification = true

-- Select the usergroups who are allowed to use any tools
SMenu.Config.IgnoreToolVerification = {"superadmin", "owner"}

-- Show all tool when the usergroup of the player is insite IgnoreToolVerification config
SMenu.Config.ShowToolIfIsVerified = true

-- Enable the FastDL
SMenu.Config.FastDl = true

--[[-----------------------------------------------------------------------------------------------
Contents are available here : https://steamcommunity.com/sharedfiles/filedetails/?id=2121357158
Thank you for using SMenu ^^

Buyer : {{ user_id }}
-----------------------------------------------------------------------------------------------]]--